import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, Link } from 'react-router';


import Normal from './pages/normal';
import Scroll from './pages/scroll';
import Overflow from './pages/overflow';
import Image from './pages/image';
import Debounce from './pages/debounce';

const Home = () => (
  <ul className="nav">
    <li><Link to="/normal">normal</Link></li>
    <li><Link to="/image">image</Link></li>
    <li><Link to="/scroll">scroll</Link></li>
   
    <li><Link to="/debounce">debounce</Link></li>
  </ul>
);

const routes = (
  <Router history={hashHistory}>
    <Route path="/" component={Home} />
    <Route path="/image" component={Image} />
    <Route path="/normal" component={Normal} />
    <Route path="/scroll" component={Scroll} />
    <Route path="/overflow" component={Overflow} />
    <Route path="/debounce" component={Debounce} />
  </Router>
);

ReactDOM.render(routes, document.getElementById('app'));
