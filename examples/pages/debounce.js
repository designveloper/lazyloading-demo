import React, { Component } from 'react';
import LazyLoad from '../../src/';
import Widget from '../components/Widget';
import { uniqueId } from '../utils';
import {heroes} from '../components/heroes'
export default class Debounce extends Component {
  constructor() {
    super();

    const id = uniqueId();

  }

  handleClick() {
    const id = uniqueId();
  }

  render() {
    return (
      <div className="wrapper">

        <div className="widget-list">
          {heroes.map((el, index) => {
            return (
              <LazyLoad key={index} debounce={500} height={400}>
                <Widget id={el.uniqueId} count={index + 1} src={el.src} name={el.name} attr={el.attribute}/>
              </LazyLoad>
            );
          })}
        </div>
      </div>
    );
  }
}

