import React, { Component } from 'react';
import LazyLoad from '../../src/';
import Widget from '../components/Widget';
import { uniqueId } from '../utils';
import {heroes} from '../components/heroes'
export default class Normal extends Component {
  constructor() {
    super();

    const id = uniqueId();
    this.state = {
      arr: Array.apply(null, Array(20)).map((a, index) => {
        return {
          uniqueId: id,
          once: [6, 7].indexOf(index) > -1
        };
      })
    };
  }

  handleClick() {
    const id = uniqueId();

    this.setState({
      arr: this.state.arr.map(el => {
        return {
          ...el,
          uniqueId: id
        };
      })
    });
  }

  render() {
    return (
      <div className="wrapper">
        <div className="widget-list">
        {heroes.map((el, index) => {
          return (
            <div key={index} >
              <Widget id={el.uniqueId} count={index + 1} src={el.src} name={el.name}/>
            </div>
          );
        })}
        </div>
      </div>
    );
  }
}

// debounce={500} height={400