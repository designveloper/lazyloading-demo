import React, { Component } from 'react';
import Lazyload from '../../src/';
import { heroes } from '../components/heroes';

export default class Image extends Component {
  render() {
    return (
      <div className="wrapper">
        <div className="widget-list image-container">
          {
            heroes.map((element, index) => {
              return (
                <Lazyload throttle={200} height={300} key={index}>
                  <img src={element.src} />
                </Lazyload>
              )
            })
          }


        </div>
      </div>
    );
  }
}

