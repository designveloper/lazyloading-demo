import React, { Component } from 'react';
import LazyLoad from '../../src/';
import Widget from '../components/Widget';
import { uniqueId } from '../utils';
import { heroes } from '../components/heroes'
import {
  Col
} from 'react-bootstrap'
export default class Scroll extends Component {
  constructor() {
    super();

    const id = uniqueId();
  }

  handleQuickJump(index, e) {
    if (e) {
      e.preventDefault();
    }
    console.log(index)
    const nodeList = document.querySelectorAll('.widget-list .widget-wrapper');
    if (nodeList[index]) {
      window.scrollTo(0, nodeList[index].getBoundingClientRect().top + window.pageYOffset);
    }
  }

  render() {
    return (

      <Col sm={12}>
        <Col sm={4}>
          <div className="quick-jump">
            <h4>Quick jump to: </h4>
            {heroes.map((el, index) => (
              <a onClick={this.handleQuickJump.bind(this, index)} key={index}>{index + 1}</a>
            ))}
          </div>
        </Col>
        <Col sm={8} className="scroll-col">
          <div className="widget-list">
            {heroes.map((el, index) => {
              return (
                <div className="widget-wrapper" key={index}>
                  <LazyLoad debounce={500} height={400}>
                    <Widget id={el.uniqueId} count={index + 1} src={el.src} name={el.name} />
                  </LazyLoad>
                </div>

              );
            })}
          </div>
        </Col>
      </Col>




    );
  }
}

