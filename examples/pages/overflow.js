import React, { Component } from 'react';
import LazyLoad from '../../src/';
import Widget from '../components/Widget';
import { uniqueId } from '../utils';
import { heroes } from '../components/heroes'
export default class Overflow extends Component {
  constructor() {
    super();
    const id = uniqueId();
  }

  render() {
    return (
      <div className="wrapper overflow-wrapper">
        <h1>LazyLoad in Overflow Container</h1>
        <div className="widget-list overflow">
          {
            heroes.map((el, index) => {              
              return (

                <LazyLoad key={index} overflow={true} height={200}>
                  <Widget id={el.uniqueId} count={index + 1} src={el.src} name={el.name} />
                </LazyLoad>
              );
            })
          }
        </div>
      </div>
    );
  }
}


